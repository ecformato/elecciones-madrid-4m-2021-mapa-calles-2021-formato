import './map.js';

import { setRRSSLinks, scrollActiv } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';

//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

//Vars
let viewportHeight = window.innerHeight;
let logoFooter = document.getElementById("logoFooter");
let sharing = document.getElementById("sharing");
let summaries = document.getElementsByClassName("content__summary");

//Ejecución
main();

function main() {
	setRRSSLinks();
}

window.addEventListener('scroll', function(){
	scrollActiv();
	
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}

	if (isMobile()) {
		if (isElementInViewport(logoFooter)) {
			sharing.classList.remove("visible")
		}
	}

	for (let i = 0; i < summaries.length; i++) {
		if(isElementInViewport(summaries[i])) {
			if(!summaries[i].classList.contains('content__summary--animated')) {
				summaries[i].classList.add('content__summary--animated')
			}
		}
	}
});


//OPEN/CLOSE FILTERS
let filtersLayer = document.getElementById('filtersLayer');
let filtersOpen = document.getElementById('filtersOpen');
let filtersClose = document.getElementById('filtersClose');

if ( isMobile() ) {
	filtersLayer.classList.add('filters--closed');
} else {
	filtersLayer.classList.add('filters--opened');
}

filtersOpen.addEventListener('click', function(){
	filtersLayer.classList.remove('filters--closed');
	filtersLayer.classList.add('filters--opened');
});

filtersClose.addEventListener('click', function(){
	filtersLayer.classList.remove('filters--opened');
	filtersLayer.classList.add('filters--closed');
});