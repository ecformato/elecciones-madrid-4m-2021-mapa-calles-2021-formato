import { text } from 'd3-request';
import { dsvFormat } from 'd3-dsv';
import { isMobile } from '../../common_projects_utilities/js/dom-helpers';

//GRADIENTES DE COLORES
let bloqueIzquierdas = ['#f2edd1', '#f4eab8', '#f5e79f', '#f6e385', '#fbdd49', '#e2c32a'],
    bloqueDerechas = ['#d3d3d1', '#b5b7b8', '#979b9f', '#787e86', '#5a626d', '#3c4554'],
    empates = '#f1f0ea',
    sin_informacion = '#e1e1e1',
    other_party_color = '#575757',
    podemos_four_colors = ['#efeaf1','#c4adc2','#966a9a','#692772'],
    masmadrid_four_colors = ['#ebf4f3','#b7ded7','#85cdc5','#20c0b2'],
    psoe_four_colors = ['#f5ebea','#f3a49c','#f4574f','#f60b01'],
    cs_four_colors = ['#f6e9e1','#f2be9f','#f38c54','#f45a09'],
    pp_four_colors = ['#eff3f6','#b9d9f0','#80c1f7','#48aafd'],
    vox_four_colors = ['#ecf2e4','#c8dfab','#a0cf6c','#77be2d'],
    participacion_colors = ['#E4F4FB','#001A2D'],
    podemos_two_colors = ['#d7a7dd','#692772'],
    masmadrid_two_colors = ['#bae8e4','#20c0b2'],
    psoe_two_colors = ['#fbc9c7','#f60b01'],
    cs_two_colors = ['#ffc09f','#f45a09'],
    pp_two_colors = ['#c6e5ff','#48aafd'],
    vox_two_colors = ['#d5e9c0','#77be2d'];

//Cambio leyendas
let legends = document.getElementsByClassName('legends')[0];
let gradientBar = document.getElementsByClassName('gradient-box__value')[0];
let maxVoteData = document.getElementById('maxVoteData');
let minVoteData = document.getElementById('minVoteData');

let filtersLayer = document.getElementById('filtersLayer');

function closeFilterLayer() {
    filtersLayer.classList.remove('filters--opened');
	filtersLayer.classList.add('filters--closed');
}

//mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syMGI5NHV4MDB1OTNnbnJtZ3UweGh5YiJ9.UrXbe3ehqGCgKdgMgHZnBQ'; //Cambiar
mapboxgl.accessToken = 'pk.eyJ1IjoiZGF0b3MtZWxjb25maWRlbmNpYWwiLCJhIjoiY2syNHNsMGN0MWRyZDNubnkwaHFiZGw4ayJ9.eKIhDFvX3YotsAEtGihnAw';

/* Ajustar ZOOM, MINZOOM Y CENTER EN FUNCIÓN DEL DISPOSITIVO */
let mapWidth = document.getElementById('map').clientWidth;
let zoom = mapWidth > 525 ? 8 : 7.5;
let minZoom = mapWidth > 525 ? 7 : 7.5;
let center = [-3.75, 40.55];
let hoveredStateId = null;

let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/datos-elconfidencial/ckmowiz7t7pg517qvw94iyz3e',
    attributionControl: false,
    zoom: zoom,
    minZoom: minZoom,
    maxZoom: 16,
    center: center
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl,
        language: 'es',
        countries: 'es',
        marker: false,
        placeholder: 'Busque por calle o municipio',
        bbox: [-4.877929,39.880235,-2.988281,41.236511],
        filter: function (item) {
            return item.context
                .map(function (i) {
                    return (
                        i.id.split('.').shift() === 'region' &&
                        i.text === 'Madrid'
                        );
                    })
                .reduce(function (acc, cur) {
                    return acc || cur;
                });
        }
    })
);

/* Variable para poder navegar sobre el mapa */
let nav = new mapboxgl.NavigationControl({showCompass:false});
map.addControl(nav, 'top-right');

/* Inicializar variable para trabajar con popups */
let popup = new mapboxgl.Popup({
    closeButton: true,
    closeOnClick: false
});

map.scrollZoom.disable();

map.on('load', function(){
    let file = 'https://raw.githubusercontent.com/CarlosMunozDiazEC/prueba-datos/main/batalla_madrid/secciones_2021_4m.csv'; //Cambiar por los de 2021

    text(file, function(data){
        let dsv = dsvFormat(";");
        let parseData = dsv.parse(data);

        map.addSource('secciones_censales', {
            'type': 'vector',
            'url': 'mapbox://datos-elconfidencial.aupbtynu',
            promoteId: 'CUSEC'
        });
        
        parseData.map((item) => {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: +item['cusec']
            }, {
                //Fuerzas políticas
                pp_porc: +item['% PP'].replace(',','.'),
                psoe_porc: +item['% PSOE'].replace(',','.'),
                masmadrid_porc: +item['% MÁS MADRID'].replace(',','.'),
                podemos_porc: +item['% Podemos'].replace(',','.'),
                vox_porc: +item['% VOX'].replace(',','.'),
                cs_porc: +item['% Cs'].replace(',','.'),

                //Otras variables
                primeraFuerza: item['Primera fuerza'].trim(),
                primeraFuerzaPorc: +item['% PF'].replace(',','.'),
                segundaFuerza: item['Segunda Fuerza'].trim(),
                segundaFuerzaPorc: +item['% SF'].replace(',','.'),

                primeraFuerza2019: item['1F 2019'].trim(),
                primeraFuerza2019Porc: +item['% 1F 2019'].replace(',','.'),

                participacion_porc: +item['Participación'].replace(',','.'),                
                bloqueGanador: item['Bloque Gana'].trim(),
                izq_porc: +item['Izquierda'].replace(',','.'),
                der_porc: +item['Derecha'].replace(',','.')
            });
        });

        //Coropletas > Primero rellenamos con ganador
        map.addLayer({
            'id': 'secciones_censales_madrid',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'fill',
            'paint': {
                'fill-color': [
                    'match',
                    ['feature-state', 'primeraFuerza'],
                    "MÁS MADRID", [
                        'step',
                        ['feature-state', 'masmadrid_porc'],
                        masmadrid_four_colors[0],
                        25,
                        masmadrid_four_colors[1],
                        40,
                        masmadrid_four_colors[2],
                        55,
                        masmadrid_four_colors[3]
                    ],
                    "Cs", [
                        'step',
                        ['feature-state', 'cs_porc'],
                        cs_four_colors[0],
                        25,
                        cs_four_colors[1],
                        40,
                        cs_four_colors[2],
                        55,
                        cs_four_colors[3]
                    ],
                    "PSOE", [
                        'step',
                        ['feature-state', 'psoe_porc'],
                        psoe_four_colors[0],
                        25,
                        psoe_four_colors[1],
                        40,
                        psoe_four_colors[2],
                        55,
                        psoe_four_colors[3]
                    ],
                    "PP", [
                        'step',
                        ['feature-state', 'pp_porc'],
                        pp_four_colors[0],
                        25,
                        pp_four_colors[1],
                        40,
                        pp_four_colors[2],
                        55,
                        pp_four_colors[3]
                    ],
                    "PODEMOS-IU", [
                        'step',
                        ['feature-state', 'podemos_porc'],
                        podemos_four_colors[0],
                        25,
                        podemos_four_colors[1],
                        40,
                        podemos_four_colors[2],
                        55,
                        podemos_four_colors[3]
                    ],
                    "VOX", [
                        'step',
                        ['feature-state', 'vox_porc'],
                        vox_four_colors[0],
                        25,
                        vox_four_colors[1],
                        40,
                        vox_four_colors[2],
                        55,
                        vox_four_colors[3]
                    ],
                    "PCOE-PCPE", other_party_color,
                    empates
                ],
                'fill-opacity': 0.75
            }      
        }, 'road-simple');

        //Límites censales
        map.addLayer({
            'id': 'secciones_censales_line',
            'source': 'secciones_censales',
            'source-layer': 'secciones_cam-a5lgu8',
            'type': 'line',
            'paint': {
                'line-color': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    '#000',
                    '#fff'
                ],
                'line-width': [
                    'case',
                    ['boolean', ['feature-state', 'hover'], false],
                    1.75,
                    0.1
                ]
            }      
        }, 'road-label-simple');

        //Eventos con botones
        document.getElementById('primeraFuerza').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend();
            handlePrimera();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('primeraFuerza2019').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend();
            handlePrimera2019();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('segundaFuerza').addEventListener('click', (e) => {
            changeButton(e.target);
            changeLegend();
            handleSegunda();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('bloqueGanador').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = 'Dcha.';
            maxVoteData.textContent = 'Izda.';
            changeLegend('bloque-ganador');
            handleBloqueGanador();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcParticipacion').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '100%';
            changeLegend('participacion');
            handleParticipacion();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcMasMadrid').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 20%';
            changeLegend('masmadrid');
            handleMasMadrid();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcPSOE').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 20%';
            changeLegend('psoe');
            handlePsoe();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcCS').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 5%';
            changeLegend('cs');
            handleCs();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcPP').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 65%';
            changeLegend('pp');
            handlePp();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcPodemos').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 10%';
            changeLegend('podemos');
            handlePodemos();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        document.getElementById('porcVOX').addEventListener('click', (e) => {
            changeButton(e.target);
            minVoteData.textContent = '0%';
            maxVoteData.textContent = '+ 10%';
            changeLegend('vox');
            handleVox();
            if (isMobile()) {
                closeFilterLayer();
            }
        });

        //Eventos de ratón
        bind_event(popup, 'secciones_censales_madrid');
        
        /*
        * Funciones asociadas a eventos en el DOM
        */
        function handlePrimera() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'primeraFuerza'],
                "MÁS MADRID", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'masmadrid_porc'],
                    0,
                    masmadrid_four_colors[0],
                    25,
                    masmadrid_four_colors[1],
                    40,
                    masmadrid_four_colors[2],
                    55,
                    masmadrid_four_colors[3]
                ],
                "Cs", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'cs_porc'],
                    0,
                    cs_four_colors[0],
                    25,
                    cs_four_colors[1],
                    40,
                    cs_four_colors[2],
                    55,
                    cs_four_colors[3]
                ],
                "PSOE", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'psoe_porc'],
                    0,
                    psoe_four_colors[0],
                    25,
                    psoe_four_colors[1],
                    40,
                    psoe_four_colors[2],
                    55,
                    psoe_four_colors[3]
                ],
                "PP", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'pp_porc'],
                    0,
                    pp_four_colors[0],
                    25,
                    pp_four_colors[1],
                    40,
                    pp_four_colors[2],
                    55,
                    pp_four_colors[3]
                ],
                "PODEMOS-IU", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'podemos_porc'],
                    0,
                    podemos_four_colors[0],
                    25,
                    podemos_four_colors[1],
                    40,
                    podemos_four_colors[2],
                    55,
                    podemos_four_colors[3]
                ],
                "VOX", [
                    'interpolate', 
                    ['linear'],
                    ['feature-state', 'vox_porc'],
                    0,
                    vox_four_colors[0],
                    25,
                    vox_four_colors[1],
                    40,
                    vox_four_colors[2],
                    55,
                    vox_four_colors[3]
                ],
                "PCOE-PCPE", other_party_color,
                empates
            ]);
        }

        function handlePrimera2019() { //Cambiar la variable de primeraFuerza
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'primeraFuerza2019'],
                    "MÁS MADRID", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        masmadrid_four_colors[0],
                        25,
                        masmadrid_four_colors[1],
                        30,
                        masmadrid_four_colors[2],
                        35,
                        masmadrid_four_colors[3]
                    ],
                    "Cs", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        cs_four_colors[0],
                        25,
                        cs_four_colors[1],
                        30,
                        cs_four_colors[2],
                        35,
                        cs_four_colors[3]
                    ],
                    "PSOE", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        psoe_four_colors[0],
                        25,
                        psoe_four_colors[1],
                        30,
                        psoe_four_colors[2],
                        35,
                        psoe_four_colors[3]
                    ],
                    "PP", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        pp_four_colors[0],
                        25,
                        pp_four_colors[1],
                        30,
                        pp_four_colors[2],
                        35,
                        pp_four_colors[3]
                    ],
                    "PODEMOS-IU", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        podemos_four_colors[0],
                        25,
                        podemos_four_colors[1],
                        30,
                        podemos_four_colors[2],
                        35,
                        podemos_four_colors[3]
                    ],
                    "VOX", [
                        'step',
                        ['feature-state', 'primeraFuerza2019Porc'],
                        vox_four_colors[0],
                        25,
                        vox_four_colors[1],
                        30,
                        vox_four_colors[2],
                        35,
                        vox_four_colors[3]
                    ],
                    "Sin datos", sin_informacion,
                    empates
            ]);
        }

        function handleSegunda() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'segundaFuerza'],
                "MÁS MADRID", [
                    'step',
                    ['feature-state', 'masmadrid_porc'],
                    masmadrid_four_colors[0],
                    15,
                    masmadrid_four_colors[1],
                    20,
                    masmadrid_four_colors[2],
                    25,
                    masmadrid_four_colors[3]
                ],
                "Cs", [
                    'step',
                    ['feature-state', 'cs_porc'],
                    cs_four_colors[0],
                    15,
                    cs_four_colors[1],
                    20,
                    cs_four_colors[2],
                    25,
                    cs_four_colors[3]
                ],
                "PSOE", [
                    'step',
                    ['feature-state', 'psoe_porc'],
                    psoe_four_colors[0],
                    15,
                    psoe_four_colors[1],
                    20,
                    psoe_four_colors[2],
                    25,
                    psoe_four_colors[3]
                ],
                "PP", [
                    'step',
                    ['feature-state', 'pp_porc'],
                    pp_four_colors[0],
                    15,
                    pp_four_colors[1],
                    20,
                    pp_four_colors[2],
                    25,
                    pp_four_colors[3]
                ],
                "PODEMOS-IU", [
                    'step',
                    ['feature-state', 'podemos_porc'],
                    podemos_four_colors[0],
                    15,
                    podemos_four_colors[1],
                    20,
                    podemos_four_colors[2],
                    25,
                    podemos_four_colors[3]
                ],
                "VOX", [
                    'step',
                    ['feature-state', 'vox_porc'],
                    vox_four_colors[0],
                    15,
                    vox_four_colors[1],
                    20,
                    vox_four_colors[2],
                    25,
                    vox_four_colors[3]
                ],
                "VOLT", other_party_color,
                "POLE", other_party_color,
                "PARTIDO AUTÓNOMOS", other_party_color,
                empates
            ]);
        }

        function handleBloqueGanador() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ['match',
                ['feature-state', 'bloqueGanador'],
                "Izquierda", [
                    "step",
                    ['feature-state', 'izq_porc'],
                    bloqueIzquierdas[0],
                    52,
                    bloqueIzquierdas[1],
                    56,
                    bloqueIzquierdas[2],
                    60,
                    bloqueIzquierdas[3],
                    64,
                    bloqueIzquierdas[4],
                    68,
                    bloqueIzquierdas[5]
                ],
                "Derecha", [
                    "step",
                    ['feature-state', 'der_porc'],
                    bloqueDerechas[0],
                    52,
                    bloqueDerechas[1],
                    56,
                    bloqueDerechas[2],
                    60,
                    bloqueDerechas[3],
                    64,
                    bloqueDerechas[4],
                    68,
                    bloqueDerechas[5]
                ],
                empates
            ]);
        }

        function handleParticipacion() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'participacion_porc'],
                    0, participacion_colors[0],
                    100, participacion_colors[1]
                ]
            );
        }

        function handleMasMadrid() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'masmadrid_porc'],
                    0, masmadrid_two_colors[0],
                    20, masmadrid_two_colors[1]
                ]
            );
        }

        function handlePsoe() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'psoe_porc'],
                    0, psoe_two_colors[0],
                    20, psoe_two_colors[1]
                ]
            );
        }

        function handleCs() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate",
                ['linear'],
                ['feature-state', 'cs_porc'],
                    0, cs_two_colors[0],
                    5, cs_two_colors[1]
                ]
            );
        }

        function handlePp() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'pp_porc'],
                    0, pp_two_colors[0],
                    65, pp_two_colors[1]
                ]
            );
        }

        function handlePodemos() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'podemos_porc'],
                    0, podemos_two_colors[0],
                    10, podemos_two_colors[1]
                ]
            );
        }

        function handleVox() {
            map.setPaintProperty('secciones_censales_madrid', 'fill-color', 
                ["interpolate", [
                    "linear", 1
                ], ['feature-state', 'vox_porc'],
                    0, vox_two_colors[0],
                    10, vox_two_colors[1]
                ]
            );
        }
    });
});

////// Eventos DOM > Efecto en Mapbox //////
//Selección de botones
let currentButton = document.querySelector('.filters__btn--active');
function changeButton(button){
    currentButton.classList.remove('filters__btn--active');
    button.classList.add('filters__btn--active');
    currentButton = button;
}

function changeLegend(legend = undefined){
    if(!legend) {
        legends.classList.remove('legends--active');
    } else {
        legends.classList.add('legends--active');

        if(gradientBar.classList.length == 2) {
            gradientBar.classList.remove(gradientBar.classList.item(1));
            gradientBar.classList.add(`gradient-box__value--${legend}`);
        } else {
            gradientBar.classList.add(`gradient-box__value--${legend}`);
        }
    } 
}


//// Otros elementos del DOM > Eventos responsive + Tooltip ////////
if(window.innerWidth < 640){
    map.dragPan.disable();
    map.on('zoom', function(){
        let zoom = map.getZoom();        
        if(zoom > 8.25){
            map.dragPan.enable();
        } else {
            map.dragPan.disable();
        }
    });
}

//Uso del tooltip
function bind_event(popup, id){
    map.on('mousemove', id, function(e){
        //Primera parte
        let propiedades = e.features[0];
        map.getCanvas().style.cursor = 'pointer';
        var coordinates = e.lngLat;       
        var tooltipText = get_tooltip_text(propiedades);
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }        
        popup.setLngLat(coordinates)
            .setHTML(tooltipText)
            .addTo(map);

        //Segunda parte
        if (e.features.length > 0) {
            if (hoveredStateId) {
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: hoveredStateId
            }, {
                hover: false
            });
            }
            hoveredStateId = e.features[0].id;
            map.setFeatureState({
                source: 'secciones_censales',
                sourceLayer: 'secciones_cam-a5lgu8',
                id: hoveredStateId
            }, {
                hover: true
            });
        }
        map.getCanvas().style.cursor = 'pointer';
    });
    map.on('mouseleave', id, function() {
        map.getCanvas().style.cursor = '';
        popup.remove();

        if (hoveredStateId) {
        map.setFeatureState({
            source: 'secciones_censales',
            sourceLayer: 'secciones_cam-a5lgu8',
            id: hoveredStateId
        }, {
            hover: false
        });
        }
        hoveredStateId = null;
    });
}  

function get_tooltip_text(propiedades){
    let caracteristicas_fijas = propiedades.properties;
    let estado = propiedades.state;

    console.log(caracteristicas_fijas, estado);

    let parties = [
        {name: 'PSOE', data21: estado.psoe_porc},
        {name: 'PP', data21: estado.pp_porc},
        {name: 'Cs', data21: estado.cs_porc},
        {name: 'M. Madrid', data21: estado.masmadrid_porc},
        {name: 'VOX', data21: estado.vox_porc},
        {name: 'Podemos', data21: estado.podemos_porc}
    ]

    let otherWinner = '';

    if (estado.primeraFuerza != 'PP' && estado.primeraFuerza != 'PSOE' && estado.primeraFuerza != 'MÁS MADRID' && estado.primeraFuerza != 'Empate') {
        otherWinner = estado.primeraFuerza;
    }

    //Creación de arrays auxiliares para evitar que las ordenaciones se sobreescriban entre sí
    let aux2021 = [...parties];
    let partyOrder2021 = aux2021.sort(comparativa2021);

    let html = `<div class="widget__title">${caracteristicas_fijas.NMUN} (${caracteristicas_fijas.CUSEC.substr(-5)})</div>`;

    if (caracteristicas_fijas.CUSEC == '2800503005' || caracteristicas_fijas.CUSEC == '2801501002' || 
        caracteristicas_fijas.CUSEC == '2805701001' || caracteristicas_fijas.CUSEC == '2807405017') {
        html += '<div style="margin-bottom: 8px">*Datos incompletos por descuadre de votos</div>';
    } else {
        html += `
        <div class="b-fuerzas">
            <div class="b-fuerzas__title">Resultados en 2021</div>
            <div class="b-fuerzas__parties">
                <div>${partyOrder2021[0].name} (${partyOrder2021[0].data21.toFixed(1).toString().replace('.',',')}%)</div>
                <div>${partyOrder2021[1].name} (${partyOrder2021[1].data21.toFixed(1).toString().replace('.',',')}%)</div>
                <div>${partyOrder2021[2].name} (${partyOrder2021[2].data21.toFixed(1).toString().replace('.',',')}%)</div>
                <div>${partyOrder2021[3].name} (${partyOrder2021[3].data21.toFixed(1).toString().replace('.',',')}%)</div>
                <div>${partyOrder2021[4].name} (${partyOrder2021[4].data21.toFixed(1).toString().replace('.',',')}%)</div>
                <div>${partyOrder2021[5].name} (${partyOrder2021[5].data21.toFixed(1).toString().replace('.',',')}%)</div>
            </div>
            ${otherWinner != '' ? `<div style="margin-bottom: 8px; margin-top: -2px;">**Ganador: ${otherWinner}**</div>` : ''}
        </div>
        <div class="bloques">
            <div class="bloques__title">Bloques</div>
            <div class="bloques__items">
                <div class="bloque__item">
                    <span>Izda.</span>
                    <div class="bloque__bar" style="width: ${(estado.izq_porc * 135) / 100}px; background: #fbdd49;"></div>
                    <span>${estado.izq_porc.toFixed(1).toString().replace('.',',')}%</span>
                </div>
                <div class="bloque__item">
                    <span>Dcha.</span>
                    <div class="bloque__bar" style="width: ${(estado.der_porc * 135) / 100}px; background: #3c4554;"></div>
                    <span>${estado.der_porc.toFixed(1).toString().replace('.',',')}%</span>
                </div>
                <div class="bloque__item">
                    <span>Otros</span>
                    <div class="bloque__bar" style="width: ${((100 - (estado.izq_porc + estado.der_porc)) * 135) / 100}px; background: #cdcdcd; "></div>
                    <span>${(100 - (estado.izq_porc + estado.der_porc)).toFixed(1).toString().replace('.',',')}%</span>
                </div>
            </div>
        </div>
            <div class="participation">
                <span class="participation__title">Participación</span>
                <span class="participation__data">${estado.participacion_porc.toFixed(1).toString().replace('.',',')}%</span>
            </div>
            <div class="ganador2019">
                <span class="ganador2019__title">Ganador 2019</span>
                <span class="ganador2019__data">${estado.primeraFuerza2019} (${estado.primeraFuerza2019Porc.toString().replace('.',',')}%)</span>
            </div>
        `;
    }
    return html; 
}

/* Helper para tooltip */
function comparativa2021(a,b){
    const datoA = a.data21;
    const datoB = b.data21;

    let comparacion = 0;
    if (datoA < datoB) {
        comparacion = 1;
    } else if (datoA >= datoB) {
        comparacion = -1;
    }
    return comparacion;
}